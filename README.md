# React project deploy to netlify using Gitlab-ci

##### Demo react app

install create-react-app
```
npm install -g create-react-app
```

create app
```
create-react-app demo
```




##### Pokedex app

[link](https://github.com/alik0211/pokedex)



## Netlify 

we are going to need to create a project from a repo from netlify and create the api key, because we are going to use the [netlify cli](https://www.netlify.com/docs/cli/).

the two VAR used for this to work are:

vars
```
NETLIFY_AUTH_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
NETLIFY_SITE_ID=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```
you can export it to the local bash and test the netlify part

###### example
first install netlify cli 
```
npm install -g netlify-cli
```
then test
```
netlify deploy --dir=./build -p -m "$(git log -1 --pretty=%B)"
```





## Gitlab-ci

we are going to need to create a .gitlab-ci.yml that dictade the CI/CD pipeline, and we need to export the [environment variables](https://docs.gitlab.com/ee/ci/variables/) to the gitlab configuration.

###### .gitlab-ci.yml example
```
stages:
- dev

dev:
  stage: dev
  image: node:latest  # docker image in use
  script:  # bash like  commands 
    - cd demo 
    - npm install -g netlify-cli
    - npm install 
    - npm run build 
    - netlify deploy  --dir=./build -p -m "$(git log -1 --pretty=%B)"  # upload to netlify

  # extra cache option to speed subsecuents builds 
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - demo/node_modules/
```




